#include "mainwindow.h"

#include "ui_mainwindow.h"
#include "config.h"

#include <QtCore/QTimer>
#include <QtCore/QCoreApplication>

#include <QtGui/QMessageBox>
#include <QtGui/QSystemTrayIcon>
#include <QtGui/QMenu>

#include <QtDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    config = new Config();
    ui->setupUi(this);
    initGUI();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete trayIcon;
    delete config;
}

void MainWindow::initGUI()
{
    ui->durationTE->setTime(config->duration);
    ui->repeatCB->setChecked(config->repeat);
    ui->popupCB->setChecked(config->popup);
    ui->popupLE->setText(config->popupMessage);

    trayIcon = new QSystemTrayIcon(QIcon(":/images/bell.png"));

    trayIcon->setToolTip(tr("Click to configure"));

    QMenu* contextMenu = new QMenu();


    QAction* muteAction = new QAction("Mute", contextMenu);
    muteAction->setCheckable(true);
    connect(muteAction, SIGNAL(toggled(bool)), this, SLOT(setMuteEnabled(bool)));
    contextMenu->addAction(muteAction);

    contextMenu->addAction("Config", this, SLOT(onShowConfig()));

    contextMenu->addSeparator();

    contextMenu->addAction("About", this, SLOT(onShowAboutDialog()));

    QAction* quitAction = new QAction("Quit", contextMenu);
    connect(quitAction, SIGNAL(triggered()), QCoreApplication::instance(), SLOT(quit()));
    contextMenu->addAction(quitAction);

    trayIcon->setContextMenu(contextMenu);

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(onTrayIconActivated(QSystemTrayIcon::ActivationReason)));
    trayIcon->show();

    connect(ui->startButton, SIGNAL(toggled(bool)), this, SLOT(startButton_toggled(bool)));
    msgBox = new QMessageBox(QMessageBox::Information, "timeout", "bla",
                                          QMessageBox::Ok, 0,
                                          Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint | Qt::WindowStaysOnTopHint);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timer_timeout()));
}

void MainWindow::onTrayIconActivated(QSystemTrayIcon::ActivationReason reason)
{
    qDebug() << "activation, reason: " << reason;
    switch(reason) {
    case QSystemTrayIcon::DoubleClick:
    case QSystemTrayIcon::Trigger:
        this->show();
        break;
    }
}

void MainWindow::startButton_toggled(bool checked)
{
    if (checked) {
        config->duration = ui->durationTE->time();
        config->repeat = ui->repeatCB->isChecked();
        config->popup = ui->popupCB->isChecked();
        config->popupMessage = ui->popupLE->text();
        msgBox->setText(config->popupMessage);
        timer->setInterval((config->duration.hour() * 3600 + config->duration.minute() * 60 + config->duration.second()) * 1000);
        timer->setSingleShot(!config->repeat);
        ui->startButton->setText("Deactivate");;
        timer->start();
    } else {
        timer->stop();
        ui->startButton->setText("Activate");
    }
}

void MainWindow::timer_timeout()
{
    if (!msgBox->isVisible()) msgBox->show();
}
