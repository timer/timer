#-------------------------------------------------
#
# Project created by QtCreator 2011-01-11T11:23:42
#
#-------------------------------------------------

QT       += core gui

TARGET = timer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    config.cpp

HEADERS  += mainwindow.h \
    config.h

FORMS    += mainwindow.ui
