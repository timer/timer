#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QtGui/QSystemTrayIcon>

class QMessageBox;
class QTimer;
class QAction;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void initGUI();

public slots:
    void onTrayIconActivated(QSystemTrayIcon::ActivationReason reason);
    void startButton_toggled(bool);
    void timer_timeout();
private:
    Ui::MainWindow *ui;
    QMessageBox* msgBox;
    QTimer* timer;
    QSystemTrayIcon* trayIcon;
    QAction* muteAction;
};

#endif // MAINWINDOW_H
