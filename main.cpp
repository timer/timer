#include <QtGui/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Mattiesworld");
    QCoreApplication::setOrganizationDomain("mattiesworld.gotdns.org");
    QCoreApplication::setApplicationName("timer");
    QCoreApplication::setApplicationVersion("0.1");

    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(false);

    MainWindow w;

    return app.exec();
}
