#ifndef CONFIG_H
#define CONFIG_H

#include <QtCore/QString>
#include <QtCore/QTime>

class Config
{
public:
    Config();
    virtual ~Config();

private:
    void read();
    void write();

public: // members
    QTime duration;
    bool repeat;
    bool popup;
    QString popupMessage;
    bool playSound;
    QString soundFilename;
    bool executeCommand;
    QString commandLine;
};

extern Config* config;

#endif // CONFIG_H
