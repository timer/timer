#include "config.h"

#include <QtCore/QSettings>
#include <QtCore/QDir>

#include <QtDebug>

Config* config;

Config::Config()
{
        read();
}

Config::~Config()
{
        write();
}

void Config::read()
{
        qDebug() << "Config::read()";
        QSettings settings;

        duration = settings.value("general/duration", "1:00").toTime();
        repeat = settings.value("general/repeat", false).toBool();
        popup = settings.value("general/popup", true).toBool();
        popupMessage = settings.value("general/popupMessage", "Timeout").toString();
}

void Config::write()
{
        qDebug() << "Config::write()";

        QSettings settings;
        settings.setValue("general/duration", duration);
        settings.setValue("general/repeat", repeat);
        settings.setValue("general/popup", popup);
        settings.setValue("general/popupMessage", popupMessage);
}
